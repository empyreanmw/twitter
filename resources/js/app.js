/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

//Data bus - acts like a global store
export const EventBus = new Vue();

//Fontawesome
import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faRetweet,
    faExternalLinkAlt,
    faHeart,
    faEnvelope,
    faBookmark,
    faList,
    faHashtag,
    faHome,
    faBell,
    faEllipsisH
} from "@fortawesome/free-solid-svg-icons";
import { faCommentDots } from "@fortawesome/free-regular-svg-icons";
import { faTwitter } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
    faRetweet,
    faExternalLinkAlt,
    faHeart,
    faEnvelope,
    faBookmark,
    faList,
    faHashtag,
    faHome,
    faBell,
    faEllipsisH,
    faTwitter,
    faCommentDots
);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.component("tweet", require("./components/tweetComponent.vue").default);
Vue.component("main-menu", require("./components/mainMenu.vue").default);
Vue.component(
    "layout-component",
    require("./components/layoutComponent.vue").default
);
Vue.component(
    "main-column",
    require("./components/mainTweetColumn.vue").default
);
Vue.component(
    "tweet-container",
    require("./components/tweetContainer.vue").default
);
Vue.component(
    "tweet-postbox",
    require("./components/tweetPostBox.vue").default
);

const app = new Vue({
    el: "#app",
    data: function() {
        return {
            user: "test"
        };
    },

    created: function() {
        axios.get("/user").then(response => {
            EventBus.$emit("userData", response.data);
        });
    }
});
