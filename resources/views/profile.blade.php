@extends('layouts.dashboard')

@section('content')
<div class="container" id="app">
    <div class="row justify-content-center h-100">
        <div class="col-3">
            <main-menu></main-menu>
        </div>
        <div class="col-6">
            <main-column :tweets="{{$tweets}}"></main-column>
        </div>
        <div class="col-3">
            <h1>test test test</h1>
        </div>
    </div>
</div>
@endsection