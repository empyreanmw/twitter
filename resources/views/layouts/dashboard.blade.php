<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="user-id"
		content="<?php echo auth()->check() ? auth()->user()->id : "";?>">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
	<main id="app" class="mx-auto">
		@yield('content')
	</main>
	<script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>