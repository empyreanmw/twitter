@extends('layouts.dashboard')

@section('content')
<layout-component :user-data="{{$user}}" :tweet-array="{{$tweets}}">
</layout-component>
@endsection