<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Reply;
use Faker\Generator as Faker;

$factory->define(Reply::class, function (Faker $faker) {
    return [
        'tweet_id' => factory(App\Tweet::class)->create()->id,
        'user_id' => factory(App\User::class)->create()->id,
        'message' => $faker->paragraph
    ];
});
