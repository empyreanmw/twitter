<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/logout', function () {
	auth()->logout();
});
Route::get('/', 'HomeController@index')->name('home');

Route::prefix('tweet')->group(function () {
	Route::post('', 'TweetsController@store');
	Route::put('{tweet}/like', 'TweetsController@like');
	Route::put('{tweet}/unlike', 'TweetsController@unLike');
	Route::post('{tweet}/retweet', 'TweetsController@retweet');
	Route::put('{tweet}/untweet', 'TweetsController@untweet');
	Route::delete('{tweet}', 'TweetsController@destroy');
	Route::get('', 'TweetsController@show');
});

Route::prefix('reply')->group(function () {
	Route::post('', 'RepliesController@store');
	Route::delete('{reply}', 'RepliesController@destroy');
	Route::get('{reply}', 'RepliesController@show');
});

Route::get('/profile/{user}', 'ProfileController@index');

Route::put('/follower/{user}/add', 'UserContoller@follow');
Route::put('/follower/{user}/remove', 'UserContoller@unfollow');

Route::get('/user', function () {
	return request()->user();
});
