<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    use Likeable, Retweetable;

    protected $guarded = [];
    protected $with = ['owner', 'likes'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function markAsRetweet($retweet)
	{
		$this->isARetweet = true;
		$this->retweetUser = User::find($retweet->user_id)->name;
		$this->created_at = $retweet->created_at;
		$this->tweetId = $retweet->retweetable_id;

		return $this;
	}

    public function getMessageAttribute($value)
    {
        return ucfirst($value);
    }
}
