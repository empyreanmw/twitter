<?php

namespace App;

trait Retweetable
{
	protected static function bootRetweetable()
	{
		static::deleting(function ($model) {
			$model->retweets->each->delete();
		});
	}

	public function retweets()
	{
		return $this->morphMany(Retweet::class, 'retweetable');
	}

	public function retweet($message = null)
	{

		return $this->retweets()->create(['user_id' => auth()->id(), 'message' => $message]);
	}

	public function untweet()
	{
		$this->retweets()->where(['user_id' => auth()->id()])->get()->each(function ($retweet) {
			$retweet->delete();
		});
	}
}
