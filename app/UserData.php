<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getAvatarAttribute()
    {
        if ($this->avatar = "avatar.png") {
            return url('storage/user/avatar.png');
        } else {
            return url("storage/user/{$this->user->uuid}/$this->avatar");
        }
    }
}
