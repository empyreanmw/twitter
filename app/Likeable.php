<?php

namespace App;

trait Likeable
{
    protected static function bootLikeable()
    {
        static::deleting(function ($model) {
            $model->likes->each->delete();
        });
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function like()
    {
        if (! $this->likes()->where(['user_id' => auth()->id()])->exists()) {
            return $this->likes()->create(['user_id' => auth()->id()]);
        }
    }

    public function unlike()
    {
        $this->likes()->where(['user_id' => auth()->id()])->get()->each(function ($like) {
            $like->delete();
        });
    }

    public function isFavorited()
    {
        return $this->favorites->where('user_id', auth()->id())->count();
    }
}
