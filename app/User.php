<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'uuid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = [
        'data'
    ];

    protected $appends = [
        'profilePath'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            UserSettings::create(['user_id' => $user->id]);
            UserData::create(['user_id' => $user->id]);
        });
    }

    public function tweets()
    {
        return $this->hasMany(Tweet::class);
    }
    
    public function settings()
    {
        return $this->hasOne(UserSettings::class);
    }

    public function data()
    {
        return $this->hasOne(UserData::class);
    }

    public function peopleWeFollow()
    {
        return $this->belongsToMany('App\User', 'follow_user', 'user_id', 'follow_id')->with('tweets');
    }

    public function retweets()
    {
        return $this->hasMany(Retweet::class);
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'follow_user', 'follow_id', 'user_id');
    }

    public function follow($id)
    {
        $this->peopleWeFollow()->syncWithoutDetaching([$id]);
    }

    public function unfollow($id)
    {
        $this->peopleWeFollow()->detach([$id]);
    }

    public function getProfilePathAttribute()
    {
        return url('profile/'.$this->name);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }
}
