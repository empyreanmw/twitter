<?php

namespace App\Policies;

use App\Tweet;
use App\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class TweetPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(Tweet $tweet)
    {
    }
    
    public function delete(User $user, Tweet $tweet)
    {
        return $user->id ===  $tweet->user_id;
    }
}
