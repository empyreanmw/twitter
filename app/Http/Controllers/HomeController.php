<?php

namespace App\Http\Controllers;

use App\Tweet;
use App\Repositories\TweetRepository;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(TweetRepository $tweets)
	{
		$user = collect(auth()->user())->except(['email', 'email_verified_at']);
		$tweets = $tweets->all(null, auth()->user());
		return view('home', compact('tweets', 'user'));
	}
}
