<?php

namespace App\Http\Controllers;

use App\User;
use App\Repositories\TweetRepository;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(User $user, TweetRepository $tweets)
    {
        $tweets = $tweets->all('App\Repositories\userTweets', $user);
        return view('profile', compact(['tweets', 'user']));
    }
}
