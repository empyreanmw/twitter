<?php

namespace App\Http\Controllers;

use App\Reply;

class RepliesController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'message' => 'min:3'
        ]);

        Reply::create([
            'message' => request('message'),
            'user_id' => auth()->id(),
            'tweet_id' => request('tweet_id')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        return $reply;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reply $reply)
    {
        $this->authorize('delete', $reply);
        
        $reply->delete();
    }
}
