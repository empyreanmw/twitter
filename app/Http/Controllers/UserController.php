<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function follow(User $user)
    {
        auth()->user()->follow($user->id);
    }

    public function unfollow(User $user)
    {
        auth()->user()->unfollow($user->id);
    }
}
