<?php

namespace App\Http\Controllers;

use App\Tweet;
use App\Like;

class TweetsController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store()
	{
		request()->validate([
			'message' => 'min:3'
		]);

		$tweet = Tweet::create([
			'message' => request('message'),
			'user_id' => auth()->id()
		]);

		return $tweet->load('owner');
	}

	public function like(Tweet $tweet)
	{
		$tweet->like();
	}

	public function unLike(Tweet $tweet)
	{
		$tweet->unLike();
	}

	public function retweet(Tweet $tweet)
	{
		return $tweet->retweet(request('message'))->retweetable;
	}

	public function untweet(Post $post)
	{
		auth()->user()->untweet($post->id);
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Tweet $tweet)
	{
		$this->authorize('delete', $tweet);

		$tweet->delete();
	}
}
