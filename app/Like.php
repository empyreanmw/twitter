<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	    protected $guarded = [];
        /**
     * Get the owning commentable model.
     */
    public function likeable()
    {
        return $this->morphTo();
    }
}
