<?php

namespace App\Repositories;

use App\Tweet;
use Illuminate\Support\Collection;

class userTweets implements TweetRepositoryInterface
{
	protected $tweetCollection;
	protected $tweets;
	protected $user;

	public function __construct()
	{
		$this->tweets = new Collection();
	}

	public function get($user)
	{
		$this->user = $user;
		return $this->collect()->filter();
	}

	protected function tweets()
	{
		return $this->user->tweets;
	}

	protected function retweets()
	{
		return auth()->user()->retweets->map(function ($retweet) {
			return Tweet::find($retweet->retweetable_id)->markAsRetweet($retweet);
		});
	}

	protected function collect()
	{
		$this->tweetCollection = collect([$this->tweets(), $this->retweets()])->reject(function ($collection) {
			return $collection->count() == 0;
		});

		return $this;
	}

	protected function filter()
	{
		$this->tweetCollection->each(function ($collection) {
			$collection->each(function ($tweet) {
				$this->tweets[] = $tweet;
			});
		});

		return $this->tweets;
	}
}
