<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\Tweet;

class peopleWeFollowTweets implements TweetRepositoryInterface
{
    protected $tweetCollection;
    protected $tweets;
    protected $user;

    public function __construct()
    {
        $this->tweets = new Collection();
    }

    public function get($user)
    {
        $this->user = $user;
        return $this->collect()->filter();
    }

    protected function tweets()
    {
        return $this->user->peopleWeFollow()->has("tweets")->get()->map(function ($user) {
            return $user->tweets;
        });
    }

    protected function retweets()
    {
        return $this->user->peopleWeFollow()->has("retweets")->get()->map(function ($user) {
            return $user->retweets->map(
                function ($retweet) {
                    return Tweet::find($retweet->retweetable_id)->markAsRetweet($retweet->user_id);
                }
                );
        });
    }

    public function collect()
    {
        $this->tweetCollection = collect([$this->tweets(), $this->retweets()])->reject(function ($collection) {
            return $collection->count() == 0;
        });

        return $this;
    }

    protected function filter()
    {
        $this->tweetCollection->each(function ($item) {
            $item->each(function ($item) {
                $item->each(function ($item) {
                    $this->tweets[] = $item;
                });
            });
        });

        return $this->tweets;
    }
}
