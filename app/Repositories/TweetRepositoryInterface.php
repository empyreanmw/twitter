<?php

namespace App\Repositories;

interface TweetRepositoryInterface
{
    public function get($user);
}
