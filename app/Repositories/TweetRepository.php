<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\Tweet;

class TweetRepository
{
    protected $tweets;
    protected $tweetCollection;
    protected $tweetClasses = [
        peopleWeFollowTweets::class,
        userTweets::class
    ];
    
    public function __construct(Collection $tweets)
    {
        $this->tweets = $tweets;
        $this->tweetCollection = new Collection();
        $this->tweets= new Collection();
    }

    public function all($class = null, $user)
    {
        return $this->buildTweetClasses($class, $user)->filter();
    }

    protected function buildTweetClasses($class, $user)
    {
        if ($class) {
            $this->tweetCollection = (new $class)->get($user);
        } else {
            foreach ($this->tweetClasses as $class) {
                $this->tweetCollection[] = (new $class)->get($user);
            }
        }
        return $this;
    }

    protected function filter()
    {
        if ($this->tweetCollection->first() instanceof Collection) {
            $this->tweetCollection->each(function ($collection) {
                $collection->each(function ($tweet) {
                    $this->tweets[] = $tweet;
                });
            });
        } else {
            $this->tweets = $this->tweetCollection;
        }

        return $this->checkIfEmpty();
    }

    protected function checkIfEmpty()
    {
        return $this->tweets->isEmpty() ? Tweet::orderBy('created_at', 'desc')->get() : $this->tweets->sortByDesc('created_at')->values();
    }
}
