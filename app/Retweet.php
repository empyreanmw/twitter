<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retweet extends Model
{
    protected $guarded = [];
    /**
     * Get the owning commentable model.
     */
    public function retweetable()
    {
        return $this->morphTo();
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function tweet()
    {
        return $this->hasOne(Tweet::class, 'id');
    }
}
