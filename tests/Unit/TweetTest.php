<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Tweet;
use App\User;
use App\Reply;
use Illuminate\Auth\Access\AuthorizationException;
use App\Repositories\TweetRepository;
use Illuminate\Database\Eloquent\Collection;

class TweetTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->tweet = factory(Tweet::class)->create()->load('owner');

        $this->user = factory(User::class)->create();
    }

    public function test_a_tweet_can_have_an_owener()
    {
        $this->assertInstanceOf(User::class, $this->tweet->owner);
    }

    public function test_a_tweet_can_have_a_reply()
    {
        $reply = factory(Reply::class)->create(['tweet_id'=>$this->tweet->id]);

        $this->assertInstanceOf(Reply::class, $reply);
    }

    public function test_only_an_owner_can_delete_their_own_tweets()
    {
        $this->withoutExceptionHandling();

        $this->expectException(\Illuminate\Auth\Access\AuthorizationException::class);

        $this->actingAs($this->user);
        
        $response = $this->delete("/tweet/{$this->tweet->id}");

        $response->assertStatus(403);
    }

    public function test_a_tweet_can_be_liked()
    {
        $this->actingAs($this->user);

        $this->tweet->like();

        $this->assertEquals(1, $this->tweet->likes->count());
    }

    public function test_a_tweet_can_be_unLiked()
    {
        $this->actingAs($this->user);

        $this->tweet->like();

        $this->assertEquals(1, $this->tweet->likes->count());
    }

    public function test_a_tweeet_can_be_retweeted()
    {
        $this->actingAs($this->user);

        $this->tweet->retweet();

        $this->assertEquals(1, $this->tweet->retweets->count());
    }

    public function test_a_tweet_can_be_untweeted()
    {
        $this->actingAs($this->user);

        $this->tweet->retweet();

        $this->tweet->untweet();

        $this->assertEquals(0, $this->tweet->retweets->count());
    }

    public function test_you_can_retweet_with_a_message()
    {
        $this->actingAs($this->user);

        $this->put("/tweet/{$this->tweet->id}/retweet", ['message' => 'this is a retweet']);

        $this->assertTrue($this->tweet->retweets[0]->message !=null);
    }

    public function test_user_should_see_their_own_tweets_retweets_and_from_people_they_follow()
    {
        $this->actingAs($this->user);

        $personWeFollow = factory(User::class)->create();

        $this->user->follow($personWeFollow->id);

        $userTweet = factory(Tweet::class)->create(['user_id' => $this->user->id]);

        $followTweet = factory(Tweet::class)->create(['user_id' => $personWeFollow->id]);

        $thirdUserTweet = factory(Tweet::class)->create();

        $this->actingAs($personWeFollow);

        $thirdUserTweet->retweet();

        $this->actingAs($this->user);
        
        $tweets = new TweetRepository(new Collection());

        $this->assertEquals(3, $tweets->all(null, $this->user)->count());
    }
}
