<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Tweet;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        
        $this->user = factory(User::class)->create();
        $this->tweet = factory(Tweet::class)->create();
    }

    public function test_user_has_a_profile()
    {
        $this->actingAs($this->user);

        $this->get("/profile/{$this->user->name}")->assertSee($this->user->name);
    }

    public function test_user_can_follow_another_user()
    {
        $followTarget = factory(User::class)->create();

        $this->user->follow($followTarget->id);

        $this->assertEquals($this->user->peopleWefollow->count(), 1);
    }

    public function test_user_can_unfollow_another_user()
    {
        $unfollowTarget = factory(User::class)->create();

        $this->user->follow($unfollowTarget->id);

        $this->user->unfollow($unfollowTarget->id);

        $this->assertEquals($this->user->peopleWefollow->count(), 0);
    }
 
    public function test_a_user_can_like_a_tweet_only_once()
    {
        $this->actingAs($this->user);

        $this->tweet->like();

        $this->tweet->like();

        $this->assertEquals(1, $this->tweet->likes->count());
    }
}
