<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Reply;
use App\User;
use App\Tweet;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReplyTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->reply = factory(Reply::class)->create()->load(['tweet', 'owner']);
        $this->user = factory(User::class)->create();
    }

    public function test_a_reply_can_have_an_owner()
    {
        $this->assertInstanceOf(User::class, $this->reply->owner);
    }

    public function test_a_reply_belongs_to_a_tweet()
    {
        $this->assertInstanceOf(Tweet::class, $this->reply->tweet);
    }

    public function test_only_an_owner_can_delete_their_own_reply()
    {
        $this->withoutExceptionHandling();

        $this->expectException(\Illuminate\Auth\Access\AuthorizationException::class);

        $this->actingAs($this->user);
        
        $response = $this->delete("/reply/{$this->reply->id}");

        $response->assertStatus(403);
    }

    public function test_a_reply_can_be_liked()
    {
        $this->actingAs($this->user);

        $this->reply->like();

        $this->assertEquals(1, $this->reply->likes->count());
    }

    public function test_a_reply_can_be_unLiked()
    {
        $this->actingAs($this->user);

        $this->reply->like();

        $this->assertEquals(1, $this->reply->likes->count());
    }
}
